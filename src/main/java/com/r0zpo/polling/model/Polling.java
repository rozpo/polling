package com.r0zpo.polling.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Polling {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "polling_id")
    private Integer id;

    private Integer owner;

    private String name;

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }
}
