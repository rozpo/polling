package com.r0zpo.polling.repository;

import com.r0zpo.polling.model.Polling;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PollingRepository extends JpaRepository<Polling, Integer> {
    @Query("select p from Polling p where p.owner = ?1")
    Iterable<Polling> findAllByOwner(Integer owner);
}