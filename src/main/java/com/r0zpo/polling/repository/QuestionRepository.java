package com.r0zpo.polling.repository;

import com.r0zpo.polling.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Integer> {
    @Query("select q from Question q where q.polling = ?1")
    Iterable<Question> findAllByPolling(Integer polling);
}
