package com.r0zpo.polling.repository;

import com.r0zpo.polling.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
    User findByLogin(String login);
    User findById(String login);
}
