package com.r0zpo.polling.repository;

import com.r0zpo.polling.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    @Query("select a from Answer a where a.question = ?1")
    Iterable<Answer> findAllByQuestion(Integer question);
    Optional<Answer> findById(Integer id);
}
