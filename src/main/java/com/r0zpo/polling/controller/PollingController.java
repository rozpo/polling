package com.r0zpo.polling.controller;

import com.r0zpo.polling.bean.AnswerForm;
import com.r0zpo.polling.bean.AnswerFormItem;
import com.r0zpo.polling.model.Answer;
import com.r0zpo.polling.model.Polling;
import com.r0zpo.polling.model.Question;
import com.r0zpo.polling.model.User;
import com.r0zpo.polling.service.AnswerService;
import com.r0zpo.polling.service.PollingService;
import com.r0zpo.polling.service.QuestionService;
import com.r0zpo.polling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class PollingController {
    @Autowired
    private PollingService pollingService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    UserService userService;


    @GetMapping(path = "/polling/all/get")
    public @ResponseBody
    Iterable<Polling> getAllPolling() {
        return pollingService.getAllPolling();
    }

    @GetMapping(value = "/polling/all")
    public ModelAndView allPolling() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        modelAndView.addObject("userList", userService.getAllUsers());
        modelAndView.addObject("useUserPollings", "use");
        modelAndView.addObject("userPollings", pollingService.findAllByOwner(user.getId()));
        modelAndView.addObject("pollingList", pollingService.getAllPolling());
        modelAndView.setViewName("polling/all");
        return modelAndView;
    }

    @GetMapping(path = "/polling/{id}/get")
    public @ResponseBody
    Optional<Polling> getPolling(@PathVariable("id") int id) {
        return pollingService.findPollingById(id);
    }

    @GetMapping(value = "/polling/{id}")
    public String getPollingForm(@PathVariable("id") int id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        model.addAttribute("pollingList", pollingService.findAllByOwner(user.getId()));
        model.addAttribute("polling", pollingService.findPollingById(id).get());
        Iterable<Question> questionList = questionService.findAllByPolling(id);
        model.addAttribute("questionList", questionList);
        AnswerForm answerForm = new AnswerForm();
        questionList.forEach(question -> {
            Iterable<Answer> answerIterable = answerService.findAllByQuestion(question.getId());
            answerIterable.forEach( answer -> {
                answerForm.addAnswer(new AnswerFormItem(question.getId(), answer.getId(), answer.getText()));
            });
        });
        model.addAttribute("answerForm", answerForm);
        return "polling/polling_id";
    }
    @GetMapping(value = "/polling/stats/{id}")
    public String getPollingVotes(@PathVariable("id") int id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        model.addAttribute("pollingList", pollingService.findAllByOwner(user.getId()));
        model.addAttribute("polling", pollingService.findPollingById(id).get());
        Iterable<Question> questionList = questionService.findAllByPolling(id);
        model.addAttribute("questionList", questionList);
        AnswerForm answerForm = new AnswerForm();
        List<Answer> answerList = new ArrayList<>();
        questionList.forEach(question -> {
            Iterable<Answer> answerIterable = answerService.findAllByQuestion(question.getId());
            answerIterable.forEach( answer -> {
                answerList.add(answer);
                System.out.println("Answer counter : " + answer.getCounter());
                answerForm.addAnswer(new AnswerFormItem(question.getId(), answer.getId(), answer.getText()));
            });
        });
        model.addAttribute("answerForm", answerForm);
        model.addAttribute("answerList", answerList);
        return "polling/stats";
    }

    @PostMapping(path = "/polling/save")
    public String savePolling(@ModelAttribute AnswerForm answerForm, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        model.addAttribute("pollingList", pollingService.findAllByOwner(user.getId()));
        answerForm.answers.forEach(form -> {

            Optional<Answer> optionalAnswer = answerService.findById(form.getId());
            if (optionalAnswer.isPresent() && form.getChecked() != null) {
                optionalAnswer.get().addCounter();
                answerService.saveAnswer(optionalAnswer.get());
            }
        });
        return "polling/save";
    }

    @PostMapping(path = "/test/polling/new")
    public @ResponseBody
    String newPolling(@RequestParam String name, @RequestParam String owner) {
        Polling polling = new Polling();
        polling.setName(name);
        polling.setOwner(Integer.parseInt(owner));
        pollingService.createPolling(polling);
        System.out.println("Added " + polling.getName());
        return "newPolling:" + polling.getName();
    }

    @GetMapping(path = "/polling/new")
    String newPollingFormGet(Model model) {
        model.addAttribute("polling", new Polling());
        return "polling/new";
    }

    @PostMapping(path = "/polling/new")
    String newPollingFormPost(@ModelAttribute Polling polling, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        polling.setOwner(user.getId());
        pollingService.createPolling(polling);
        return "/dashboard";
    }

    @GetMapping(path = "/polling/all/{owner}")
    public @ResponseBody
    Iterable<Polling> getAllPollingByOwner(@PathVariable("owner") Integer owner) {
        return pollingService.findAllByOwner(owner);
    }
}
