package com.r0zpo.polling.controller;

import com.r0zpo.polling.model.Answer;
import com.r0zpo.polling.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AnswerController {
    @Autowired
    private AnswerService answerService;

    @PostMapping(path = "/test/answer/add")
    public @ResponseBody
    String addAnswer(@RequestParam String text, @RequestParam Integer question) {
        Answer answer = new Answer();
        answer.setText(text);
        answer.setQuestion(question);
        answer.resetCounter();
        answerService.addAnswer(answer);
        System.out.println("Added " + answer.getText());
        return "newQuestion:" + answer.getText() + "\n";
    }
}
