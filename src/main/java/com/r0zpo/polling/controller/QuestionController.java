package com.r0zpo.polling.controller;

import com.r0zpo.polling.bean.AnswerForm;
import com.r0zpo.polling.bean.AnswerFormItem;
import com.r0zpo.polling.model.Answer;
import com.r0zpo.polling.model.Question;
import com.r0zpo.polling.service.AnswerService;
import com.r0zpo.polling.service.PollingService;
import com.r0zpo.polling.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class QuestionController {
    @Autowired
    private PollingService pollingService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;

    @PostMapping(path = "/test/question/add")
    public @ResponseBody
    String addQuestion(@RequestParam String text, @RequestParam Integer pollingId) {
        Question question = new Question();
        question.setText(text);
        question.setPolling(pollingId);
        questionService.addQuestion(question);
        System.out.println("Added " + question.getText());
        return "newQuestion:" + question.getText() + "\n";
    }

    @GetMapping(path = "/polling/{id}/add_question")
    String addQuestionForm(@PathVariable("id") int id, Model model) {
        model.addAttribute("polling", pollingService.findPollingById(id));
        Question question = new Question();
        question.setPolling(id);
        model.addAttribute("question", question);
        AnswerForm answerForm = new AnswerForm();
        for (int i = 0; i < 4; i++) {
            answerForm.addAnswer(new AnswerFormItem());
        }
        model.addAttribute("answerForm", answerForm);
        return "/question/add";
    }

    @PostMapping(path = "/polling/add_question")
    String AddQuestionFormPost(@ModelAttribute Question question, @ModelAttribute AnswerForm answerForm, Model model) {
        questionService.addQuestion(question);

        answerForm.answers.forEach(answerFormItem -> {
            Answer answer = new Answer();
            answer.setQuestion(question.getId());
            answer.setText(answerFormItem.text);
            answer.resetCounter();
            answer.addCounter();
            answerService.saveAnswer(answer);
        });
        return "/dashboard";
    }
}
