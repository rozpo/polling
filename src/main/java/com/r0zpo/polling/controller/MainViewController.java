package com.r0zpo.polling.controller;

import com.r0zpo.polling.model.User;
import com.r0zpo.polling.service.PollingService;
import com.r0zpo.polling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainViewController {
    @Autowired
    UserService userService;
    @Autowired
    private PollingService pollingService;

    @GetMapping(value = "/")
    public ModelAndView mainView() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        modelAndView.addObject("welcome", "Welcome " + user.getFirstName() + " " + user.getLastName() +", your pollings: ");
        modelAndView.addObject("pollingList", pollingService.findAllByOwner(user.getId()));
        modelAndView.setViewName("dashboard");
        return modelAndView;
    }
}
