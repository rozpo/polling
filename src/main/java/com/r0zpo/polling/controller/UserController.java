package com.r0zpo.polling.controller;

import com.r0zpo.polling.model.User;
import com.r0zpo.polling.service.PollingService;
import com.r0zpo.polling.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    PollingService pollingService;

    @GetMapping(value = "/users/my_account")
    public ModelAndView myAccount() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        modelAndView.addObject("user", user);
        modelAndView.addObject("pollingList", pollingService.findAllByOwner(user.getId()));
        modelAndView.setViewName("users/my_account");
        return modelAndView;
    }

    @PostMapping(value = "/users/my_account")
    public ModelAndView updateAccount(@Valid User user) {
        ModelAndView modelAndView = new ModelAndView();
        if (userService.findUserById(user.getId()).isPresent()) {
            User currentUser = userService.findUserById(user.getId()).get();

            System.out.println("current Password:" + currentUser.getPassword());
            System.out.println("new Password:" + user.getPassword());
            boolean savePass = true;
            if (user.getPassword().equals("")) {
                savePass = false;
                user.setPassword(currentUser.getPassword());
            }

            userService.updateUser(user, savePass);
        }
        modelAndView.addObject("user", user);
        modelAndView.setViewName("users/my_account");
        return modelAndView;
    }

    @GetMapping(value = "/users/all")
    public ModelAndView allUsers() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("usersList", userService.getAllUsers());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByLogin(authentication.getName());
        modelAndView.addObject("user", user);
        modelAndView.addObject("pollingList", pollingService.findAllByOwner(user.getId()));
        modelAndView.setViewName("users/all");
        return modelAndView;
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody
    Optional<User> getUser(@PathVariable("id") int id) {
        return userService.findUserById(id);
    }

    @RequestMapping(path = "/users/{id}")
    String getUserForm(Model model, @PathVariable("id") String id) {
        Optional<User> user = userService.findUserById(Integer.parseInt(id));
        model.addAttribute("user", user);
        return "users/user_id";
    }
}
