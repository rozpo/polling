package com.r0zpo.polling.service;

import com.r0zpo.polling.model.Answer;
import com.r0zpo.polling.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AnswerService {
    @Autowired
    AnswerRepository answerRepository;

    public void addAnswer(Answer answer) {
        answerRepository.save(answer);
    }

    public Iterable<Answer> findAllByQuestion(Integer question) {
        return answerRepository.findAllByQuestion(question);
    }

    public Optional<Answer> findById(Integer id) {
        return answerRepository.findById(id);
    }

    public Answer saveAnswer(Answer answer) {
        return answerRepository.save(answer);
    }
}
