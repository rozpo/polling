package com.r0zpo.polling.service;

import com.r0zpo.polling.model.Polling;
import com.r0zpo.polling.repository.PollingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PollingService {
    private final PollingRepository pollingRepository;

    @Autowired
    public PollingService(PollingRepository pollingRepository) {
        this.pollingRepository = pollingRepository;
    }

    public Optional<Polling> findPollingById(Integer id) {
        return pollingRepository.findById(id);
    }

    public void createPolling(Polling polling) {
        pollingRepository.save(polling);
    }

    public Iterable<Polling> getAllPolling() {
        return pollingRepository.findAll();
    }

    public Iterable<Polling> findAllByOwner(Integer owner) {
        return pollingRepository.findAllByOwner(owner);
    }
}
