package com.r0zpo.polling.service;

import com.r0zpo.polling.model.Role;
import com.r0zpo.polling.model.User;
import com.r0zpo.polling.repository.RoleRepository;
import com.r0zpo.polling.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public Optional<User> findUserById(Integer id) {
        return userRepository.findById(id);
    }

    public User saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Role role = roleRepository.findByRole("owner");
        user.setRoles(new HashSet<Role>(Arrays.asList(role)));
        return userRepository.save(user);
    }

    public User updateUser(User user, boolean updatePassword) {
        if (updatePassword) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        }
        user.setActive(true);
        Role role = roleRepository.findByRole("owner");
        user.setRoles(new HashSet<Role>(Arrays.asList(role)));
        return userRepository.save(user);
    }

    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }
}
