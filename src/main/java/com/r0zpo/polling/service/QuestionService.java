package com.r0zpo.polling.service;

import com.r0zpo.polling.model.Question;
import com.r0zpo.polling.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {
    private QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public void addQuestion(Question question) {
        questionRepository.save(question);
    }

    public Iterable<Question> findAllByPolling(Integer polling) {
        return questionRepository.findAllByPolling(polling);
    }
}
