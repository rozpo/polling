-- db config
use db_polling;

truncate role;
truncate answer;
truncate question;
truncate polling;
truncate user;
truncate user_roles;

-- user
insert into user values(1, 'admin@admin.com', 'Admin', 1, 'Nimda', 'admin', '$2a$10$NNax93UNwGQ0bMAURRmSxeW3J5faFuG/8twhiN2yMqa0RMIAd991W');
insert into user values(2, 'parker@gmail.com', 'Peter', 1, 'Parker', 'piter', '$2a$10$5ajuxQf2WGGt8BGdWF2MNOOUS2TNVp2uoaj9NBqOgub3Hgpy58jKu');
-- roles
insert into role values (1, 'owner');
insert into role values (2, 'admin');
insert into role values (3, 'pollster');
insert into role values (4, 'user');
--user_roles
insert into user_roles values(1,1);
insert into user_roles values(2,4);
-- polling
insert into polling values (1, 'sports', 1);
insert into polling values (2, 'movies', 1);
insert into polling values (3, 'sciences', 2);
-- question
insert into question values(1, 1, 'Ulubiony rodzaj sportu');
insert into question values(2, 1, 'Rodzaj sportu ktory lubisz uprawiac');
insert into question values(3, 1, 'Najbardziej przydatne akcesoria sportowe');
insert into question values(4, 2, 'Ulubiony gatunek filmu');
insert into question values(5, 2, 'Ulubiony aktor');
insert into question values(6, 2, 'Ulubiona aktorka');
insert into question values(7, 3, 'Ulubiona dziedzina nauki');
insert into question values(8, 3, 'Ulubiony naukowiec');
insert into question values(9, 3, 'Najbardziej przydatne odkrycie naukowe');
-- answer
insert into answer values(1, 1, 1, 'Druzynowe');
insert into answer values(2, 1, 1, 'Indywidualne');
insert into answer values(3, 1, 2, 'Pilka nozna');
insert into answer values(4, 1, 2, 'Siatkowka');
insert into answer values(5, 1, 2, 'Rugby');
insert into answer values(6, 1, 2, 'Tenis');
insert into answer values(7, 1, 3, 'Suple');
insert into answer values(8, 1, 3, 'Obuwie');
insert into answer values(9, 1, 3, 'Ochraniacze');
insert into answer values(10, 1, 3, 'Nakrycia glowy');
insert into answer values(11, 1, 4, 'Akcji');
insert into answer values(12, 1, 4, 'Horror');
insert into answer values(13, 1, 4, 'Dramat');
insert into answer values(14, 1, 4, 'Romans');
insert into answer values(15, 1, 5, 'Brad Pitt');
insert into answer values(16, 1, 5, 'Leonardo Dicaprio');
insert into answer values(17, 1, 5, 'Al Pacino');
insert into answer values(18, 1, 6, 'Jessica Alba');
insert into answer values(19, 1, 6, 'Scarlet Johannson');
insert into answer values(20, 1, 7, 'Informatyka');
insert into answer values(21, 1, 7, 'Matematyka');
insert into answer values(22, 1, 7, 'Biologia');
insert into answer values(23, 1, 8, 'Thomas Edison');
insert into answer values(24, 1, 8, 'Newton');
insert into answer values(25, 1, 8, 'Gottfried Wilhelm Leibniz');
insert into answer values(26, 1, 9, 'System binarny');
insert into answer values(27, 1, 9, 'Elektrodynamika kwantowa');
insert into answer values(28, 1, 9, 'Mikroprocesor');