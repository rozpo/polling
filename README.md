# Polling Platform (Spring Boot framework)

## 1 Opis projektu
Aplikacja webowa pozwalająca na przeprowadzanie anonimowych ankiet.  
Wykorzystane technologie : 
- Spring Boot
- Bootstrap
- Thymeleaf
- MySql
- HTML5/CSS

## 2 Założenia projektowe
- Rejestracja użytkowników
- Logowanie
- Autoryzacja użytkownika
- Edycja danych konta
- Przeglądanie ankiet dostępnych na platformie
- Monitorowanie i wyświetlanie oddanych głosów
- Wypełnianie ankiety
- Tworzenie nowych ankiet

## 3 Uruchomienie aplikacji
- komenda: `./mvnw spring-boot:run`
- utworzenie paczki: `./mvnw clean package`
- uruchomienie jar: `java -jar target/polling-0.0.1-SNAPSHOT.jar`

## 4 Konfiguracja MySql
- intalacja: `sudo apt install mysql-server`
- uruchomienie konfiguracji: `sudo mysql_secure_installation`
- login: `sudo mysql --password`
- stworzenie bazy danych: `create database db_polling;`
- utworzenie użytkownia do bazy danych: `create user 'springuser'@'%' identified by 'springpassword';`
- nadanie przywilejów użytkownikowi: `grant all on db_polling.* to 'springuser'@'%';`
